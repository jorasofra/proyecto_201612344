﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Diproma.Modelos;

namespace Diproma.AccesoADatos
{
    public class EmpleadoAD
    {
        #region "Singleton"
        private static EmpleadoAD adEmpleado = null;
        private EmpleadoAD() { }
        public static EmpleadoAD getInstance()
        {
            if (adEmpleado == null)
            {
                adEmpleado = new EmpleadoAD();
            }
            return adEmpleado;
        }
        #endregion

        public void AgregarEmpleado(Modelos.Empleado empleado)
        {
            int nit = empleado.Nit_empleado;
            string nombre = empleado.Nombre;
            string apellido = empleado.Apellido;
            string nacimiento = empleado.Fecha_nacimiento.Date.ToString("dd/MM/yyyy");
            string direccion = empleado.Direccion;
            string telefono_dom = empleado.Telefono_domicilio;
            string telefono_cel = empleado.Telefono_celular;
            string email = empleado.Email;
            string pass = empleado.Pass;
            //int nit_jefe = empleado.Jefe.Nit_empleado;
            int cod_rol = empleado.Rol_Empleado.Cod_rol;

            SqlConnection con = null;
            SqlDataAdapter adapter = null;
            DataTable dt = null;

            try
            {
                string consulta = "INSERT INTO Empleado VALUES (" + nit + ", '" + nombre + "', '" + apellido + "', CONVERT(DATE, '" + nacimiento + "'), " +
                    "'" + direccion + "', '" + telefono_dom + "', '" + telefono_cel + "', '" + email + "', '" + pass + "', " + "NULL" + ", " + cod_rol + ")";
                con = Conexion.getInstance().ConexionBD();
                con.Open();
                adapter = new SqlDataAdapter(consulta, con);
                dt = new DataTable();
                adapter.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        } 

        public DataTable MostrarUsuarios()
        {
            SqlConnection con = null;
            SqlDataAdapter adapter = null;
            DataTable dataTable = null;
            try
            {
                string consulta = "SELECT * FROM Empleado;";
                con = Conexion.getInstance().ConexionBD();
                con.Open();
                adapter = new SqlDataAdapter(consulta, con);
                dataTable = new DataTable();
                adapter.Fill(dataTable);
                return dataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

    }
}