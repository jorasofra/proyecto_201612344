﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Diproma.AccesoADatos
{
    public class Conexion
    {
        #region "Singleton"
        private static Conexion con = null;

        private Conexion() { }

        public static Conexion getInstance()
        {
            if(con == null)
            {
                con = new Conexion();
            }
            return con;
        }
        #endregion

        public SqlConnection ConexionBD()
        {
            SqlConnection conexion = new SqlConnection();
            conexion.ConnectionString = "Data Source = DESKTOP-H1Q1SQ9; Initial Catalog = Diproma; Integrated Security = True";
            return conexion;
        }
    }
}