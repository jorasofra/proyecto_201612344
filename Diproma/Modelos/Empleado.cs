﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Diproma.Modelos
{
    public class Empleado
    {
        private int nit_empleado;
        private string nombre;
        private string apellido;
        private DateTime fecha_nacimiento;
        private string direccion;
        private string telefono_domicilio;
        private string telefono_celular;
        private string email;
        private string pass;
        private Empleado jefe;
        private Rol_Empleado rol_Empleado;

        public int Nit_empleado { get => nit_empleado; set => nit_empleado = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellido { get => apellido; set => apellido = value; }
        public DateTime Fecha_nacimiento { get => fecha_nacimiento; set => fecha_nacimiento = value; }
        public string Direccion { get => direccion; set => direccion = value; }
        public string Telefono_domicilio { get => telefono_domicilio; set => telefono_domicilio = value; }
        public string Telefono_celular { get => telefono_celular; set => telefono_celular = value; }
        public string Email { get => email; set => email = value; }
        public string Pass { get => pass; set => pass = value; }
        public Empleado Jefe { get => jefe; set => jefe = value; }
        public Rol_Empleado Rol_Empleado { get => rol_Empleado; set => rol_Empleado = value; }

        public Empleado(int nit_empleado, string nombre, string apellido, DateTime fecha_nacimiento, string direccion, 
            string telefono_domicilio, string telefono_celular, string email, string pass, Empleado jefe, Rol_Empleado rol_Empleado)
        {
            this.Nit_empleado = nit_empleado;
            this.Nombre = nombre;
            this.Apellido = apellido;
            this.Fecha_nacimiento = fecha_nacimiento;
            this.Direccion = direccion;
            this.Telefono_domicilio = telefono_domicilio;
            this.Telefono_celular = telefono_celular;
            this.Email = email;
            this.Pass = pass;
            this.Jefe = jefe;
            this.Rol_Empleado = rol_Empleado;
        }

        public Empleado() { }

    }
}