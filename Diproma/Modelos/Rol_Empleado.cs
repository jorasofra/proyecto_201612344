﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Diproma.Modelos
{
    public class Rol_Empleado
    {
        private int cod_rol;
        private string nombre_rol;

        public Rol_Empleado(int cod_rol, string nombre_rol)
        {
            this.cod_rol = cod_rol;
            this.nombre_rol = nombre_rol;
        }

        public int Cod_rol { get => cod_rol; set => cod_rol = value; }
        public string Nombre_rol { get => nombre_rol; set => nombre_rol = value; }
    }
}