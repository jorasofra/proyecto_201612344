﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Diproma.Modelos;
using Diproma.AccesoADatos;

namespace Diproma.Vistas
{
    public partial class AdministrarUsuarios : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ActualizarTabla();
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            Modelos.Empleado objEmpleado = new Empleado();
            objEmpleado.Nit_empleado = Convert.ToInt32(txtNit.Text);
            objEmpleado.Nombre = txtNombre.Text;
            objEmpleado.Apellido = txtApellido.Text;
            objEmpleado.Fecha_nacimiento = Convert.ToDateTime(txtFechaNacimiento.Text);
            objEmpleado.Direccion = txtDireccion.Text;
            objEmpleado.Telefono_domicilio = txtTelDomicilio.Text;
            objEmpleado.Telefono_celular = txtCelular.Text;
            objEmpleado.Email = txtEmail.Text;
            objEmpleado.Pass = txtPass.Text;
            objEmpleado.Jefe = null;
            objEmpleado.Rol_Empleado = new Rol_Empleado(2, "admin");

            AccesoADatos.EmpleadoAD.getInstance().AgregarEmpleado(objEmpleado);
            Response.Write("Agregado");
            ActualizarTabla();
        }

        private void ActualizarTabla()
        {
            tbUsuarios.DataSource = EmpleadoAD.getInstance().MostrarUsuarios();
            tbUsuarios.DataBind();
        }
    }
}