﻿<%@ Page Title="Adminstrar Usuarios" Language="C#" MasterPageFile="~/Paginas Maestras/Dashboard.Master" AutoEventWireup="true" CodeBehind="AdministrarUsuarios.aspx.cs" Inherits="Diproma.Vistas.AdministrarUsuarios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <!--CONTIENE LA BARRA DE MENÚ DEL LADO IZQUIERDO-->
    <ul class="nav flex-column">
        <li class="nav-item">
            <a class="nav-link" href="Dashboard_Administrador.aspx">
                <span data-feather="home"></span>
                Inicio <span class="sr-only">(current)</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="AdministrarUsuarios.aspx">
                <span data-feather="users"></span>
                Usuarios
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="ControlMetas.aspx">
                <span data-feather="bar-chart"></span>
                Metas
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="ControlVehiculos.aspx">
                <span data-feather="navigation"></span>
                Vehiculos
            </a>
        </li>
    </ul>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Administrar Usuarios</h1>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-md-2 mb-3">
                <label for="nit">NIT</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="txtNit"></asp:TextBox>
            </div>
            <div class="col-md-4 mb-3">
                <label for="nombre">Nombre</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="txtNombre"></asp:TextBox>
            </div>
            <div class="col-md-4 mb-3">
                <label for="apellidos">Apellidos</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="txtApellido"></asp:TextBox>
            </div>
            <div class="col-md-2 mb-3">
                <label for="rol">Seleccionar Rol</label>
                <asp:DropDownList runat="server" CssClass="btn btn-sm btn-outline-primary dropdown-toggle" ID="cbRol">
                    <asp:ListItem Value="adm">Administrador</asp:ListItem>
                    <asp:ListItem Value="ger">Gerente</asp:ListItem>
                    <asp:ListItem Value="sup">Supervisor</asp:ListItem>
                    <asp:ListItem Value="vend">Vendedor</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class="row">
            <div class="col-md-7 mb-3">
                <label for="direccion">Direccion</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="txtDireccion"></asp:TextBox>
            </div>
            <div class="col-md-2 mb-3">
                <label for="jefe">NIT Jefe</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="txtJefe"></asp:TextBox>
            </div>
            <div class="col-md-3 mb-3">
                <label for="nacimiento">Fecha de Nacimiento</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="txtFechaNacimiento" TextMode="Date"></asp:TextBox>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3 mb-3">
                <label for="teldomicilio">Telefono Domiciliar</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="txtTelDomicilio"></asp:TextBox>
            </div>
            <div class="col-md-3 mb-3">
                <label for="celular">Telefono Celular</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="txtCelular"></asp:TextBox>
            </div>
            <div class="col-md-3 mb-3">
                <label for="email">Email</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="txtEmail"></asp:TextBox>
            </div>
            <div class="col-md-3 mb-3">
                <label for="pass">Contraseña</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="txtPass" TextMode="Password"></asp:TextBox>
            </div>
        </div>

        <asp:Button runat="server" ID="btnAgregar" Text="Agregar Usuario" CssClass="btn btn-success btn-lg btn-block" OnClick="btnAgregar_Click" />
    </div>

    <hr class="mb-4" />

    <h3 class="h4">Lista de Usuarios</h3>

    <div class="table-responsive">
        <asp:GridView runat="server" ID="tbUsuarios" CssClass="table table-hover table-striped table-sm">
            <Columns>
                <asp:BoundField DataField="nit_empleado" HeaderText="NIT" />
                <asp:BoundField DataField="nombre" HeaderText="Nombre" />
                <asp:BoundField DataField="apellido" HeaderText="Apellido" />
                <asp:BoundField DataField="fecha_nacimiento" HeaderText="Nacimiento" />
                <asp:BoundField DataField="direccion" HeaderText="Direccion" />
                <asp:BoundField DataField="telefono_domicilio" HeaderText="Tel. Domicilio" />
                <asp:BoundField DataField="telefono_celular" HeaderText="Celular" />
                <asp:BoundField DataField="email" HeaderText="Correo" />
                <asp:BoundField DataField="pass" HeaderText="Clave" />
                <asp:BoundField DataField="nit_jefe" HeaderText="Jefe" />
                <asp:BoundField DataField="cod_rol" HeaderText="Rol" />
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
