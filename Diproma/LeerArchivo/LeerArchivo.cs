﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace Diproma.LeerArchivo
{
    public class LeerArchivo
    {
        public void Leer(string direccion)
        {
            //Abre el archivo y lo carga
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(direccion);

            //Lee cada etiqueta
            XmlNodeList listaNodos = xDoc.GetElementsByTagName("definicion");
            XmlNodeList listaCategoria = ((XmlElement)listaNodos[0]).GetElementsByTagName("categoria");
            XmlNodeList listaProducto = ((XmlElement)listaNodos[0]).GetElementsByTagName("producto");
            XmlNodeList listaListas = ((XmlElement)listaNodos[0]).GetElementsByTagName("lista");
            XmlNodeList listaCiudad = ((XmlElement)listaNodos[0]).GetElementsByTagName("ciudad");
            XmlNodeList listaDeparatamento = ((XmlElement)listaNodos[0]).GetElementsByTagName("depto");
            XmlNodeList listaCliente = ((XmlElement)listaNodos[0]).GetElementsByTagName("cliente");
            XmlNodeList listaPuesto = ((XmlElement)listaNodos[0]).GetElementsByTagName("puesto");
            XmlNodeList listaEmpleado = ((XmlElement)listaNodos[0]).GetElementsByTagName("empleado");
            XmlNodeList listaMeta = ((XmlElement)listaNodos[0]).GetElementsByTagName("meta");

            //Envío para el analisis e incersión de datos
            IngresarCategorias(listaCategoria);
            IngresarProductos(listaProducto);
            IngresarListasProductos(listaListas);
            IngresarCiudades(listaCiudad);
            IngresarDepartamentos(listaDeparatamento);
            IngresarClientes(listaCliente);
            IngresarPuestos(listaPuesto);
            IngresarEmpleados(listaEmpleado);
            IngresarMetas(listaMeta);
        }

        private void IngresarCategorias(XmlNodeList listaCategorias)
        {
            foreach (XmlElement nodo in listaCategorias)
            {
                int i = 0;
                XmlNodeList codigo = nodo.GetElementsByTagName("codigo");
                XmlNodeList nombre = nodo.GetElementsByTagName("nombre");
                string cod = codigo[i].InnerText;
                string name = nombre[i++].InnerText;
            }
        }

        private void IngresarProductos(XmlNodeList listaProductos)
        {

        }

        private void IngresarListasProductos(XmlNodeList listaProductos)
        {

        }

        private void IngresarCiudades(XmlNodeList listaCiudades)
        {

        }

        private void IngresarDepartamentos(XmlNodeList listaDeparamentos)
        {

        }

        private void IngresarClientes(XmlNodeList listaClientes)
        {

        }

        private void IngresarPuestos(XmlNodeList listaPuestos)
        {

        }

        private void IngresarEmpleados(XmlNodeList listaEmpleados)
        {

        }

        private void IngresarMetas(XmlNodeList listaMetas)
        {

        }

    }
}